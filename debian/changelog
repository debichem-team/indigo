indigo (1.2.3-4) UNRELEASED; urgency=medium

  * Acknowledge NMU.
  * Fix and modernize debian/watch.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Fri, 08 Jan 2021 23:19:33 +0100

indigo (1.2.3-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use the correct API to fix FTBFS. (Closes: #963305)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Wed, 22 Jul 2020 23:41:31 +0100

indigo (1.2.3-3) unstable; urgency=medium

  * No-change source-only upload.

 -- Stuart Prescott <stuart@debian.org>  Fri, 17 Jan 2020 16:54:17 +1100

indigo (1.2.3-2) unstable; urgency=medium

  * Team Upload

  [ Stuart Prescott ]
  * Add Python 3 package; remove Python 2 package (Closes: #936733).
  * Update Standards-Version to 4.4.1 (no changes required).
  * Add Rules-Requires-Root: no.
  * Replace Priority: extra with optional.
  * Update to debhelper-compat (= 12).

  [ Andrius Merkys ]
  * Updating homepage link (Closes: #944316).

 -- Stuart Prescott <stuart@debian.org>  Fri, 10 Jan 2020 17:32:04 +1100

indigo (1.2.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/library_dir_and_name.patch: Updated.
  * debian/watch: Updated.
  * debian/patches/xlocale_ftbfs_fix.patch: New patch, removing a xlocale.h
    include; this header is no longer shipped in glibc. (Closes: 887777)
  * debian/indigo-utils.install: Update directories.
  * debian/patches/pixman_fix_library_name.patch: New patch, fixes pixman
    library name hardcoding issues in Makefiles.
  * debian/patches/indigo-deco_build_fix.patch: New patch, fixes unresovled
    symbols errors while linking indigo-deco.

 -- Michael Banck <mbanck@debian.org>  Tue, 01 Jan 2019 12:01:39 +0100

indigo (1.1.12-2) unstable; urgency=medium

  * Team upload.
  * Fix build with dpkg-buildpackage -A
    Closes: #806049

 -- Andreas Tille <tille@debian.org>  Sun, 01 Jan 2017 08:33:00 +0100

indigo (1.1.12-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Change libpng12-dev build-dependency to libpng-dev, to ease libpng
    transition. (Closes: #810172)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 22 Jan 2016 11:14:50 +0100

indigo (1.1.12-1) unstable; urgency=low

  * New upstream release.

  [ Daniel Leidert ]
  * debian/control (Standards-Version): Bumped to 3.9.5.
    (Recommends): indigo-utils recommends libindigo-java because chemdiff
    needs it.
  * debian/chemdiff.1, debian/indigo-cano.1, debian/indigo-deco.1,
   debian/indigo-depict.1, debian/indigo-utils.manpages: Added.
  * debian/patches/718689_gcc_architecture_flags.patch: Dropped.
    - Applied upstream.
  * debian/patches/series: Adjusted.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Tue, 11 Feb 2014 22:35:34 +0100

indigo (1.1.11-2) unstable; urgency=low

  [ Daniel Leidert ]
  * debian/javabuild, debian/libindigo-java.jlibs, debian/rules: Removed
    upstream version from jars - handled by javahelper itself.
  * debian/patches/718689_gcc_architecture_flags.patch: Added.
    - Only apply GCC flags -m32/-m64 to architectures which support them
      and don't include the GCC hack on kfreebsd-amd64 (closes: #718689).
  * debian/patches/series: Adjusted.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 04 Aug 2013 20:56:28 +0200

indigo (1.1.11-1) unstable; urgency=low

  * New upstream release.
    - Uses Hill convention for GrossFormula function (closes: #690275).

  [ Daniel Leidert ]
  * debian/control (Standards-Version): Bumped to 3.9.4.
    (Build-Depends): Added python (closes: #713767). Added autotools-dev,
    libfreetype6-dev and libpng12-dev.
    (Vcs-Browser, Vcs-Svn): Fixed vcs-field-not-canonical.
    (Section): Fixed duplication of sections.
    (Depends): Fixed weak-library-dev-dependency. Hardened dependency of
    python-indigo and libindigo-java to libindigo0d.
    (Description): Fixed typos.
  * debian/libindigo0d.dirs, debian/libindigo-dev.dirs: Dropped.
  * debian/libindigo0d.install: Added libraries.
  * debian/libindigo-dev.install: Added libraries.
  * debian/libindigo0d.lintian-overrides: Added to override
    package-name-doesnt-match-sonames.
  * debian/rules: Enabled hardening. Added autotools_dev addon.
    (override_dh_auto_configure): Added -DCMAKE_BUILD_TYPE and
    -DCMAKE_SKIP_RPATH to avoid setting RPATHs. Define missing library paths
    for system libraries.
    (override_dh_auto_install): Moved library installation to dh_install.
    (override_dh_makeshlibs): Dropped version. Better use -V only.
  * debian/README.source: Updated.
  * debian/patches/library_dir_and_name.patch: Updated.
    - Set the libraries SOVERSION to "0d" to fix inter-library linkages. Fixed
      some includes.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sat, 03 Aug 2013 19:33:00 +0200

indigo (1.1.5-1) UNRELEASED; urgency=low

  * New upstream release.

  [ Michael Banck ]
  * debian/control (indigo-utils/Description): Add a one-line summary for
    every tool explaining its purpose.
  * debian/control (Build-Depends): Added cmake.
  * debian/rules: Overhauled for cmake and directory rearrangements.
  * debian/rules (CLASSPATH): Updated version numbers.
  * debian/libindigo-java.jlibs: Likewise.
  * debian/javabuild: Likewise.
  * debian/patches/makefile.patch: Removed, no longer needed.
  * debian/patches/java_wrappers.patch: Updated.
  * debian/patches/library_dir_and_name.patch: Likewise.
  * debian/indigo-utils.install: Updated for new cmake build directory.
  * debian/libindigo-dev.install: Likewise.
  * debian/libindigo-dev.dirs: New file.
  * debian/rules (override_dh_makeshlibs): New rule, set libindigo0d
    shlibs versioning information to ">= 1.1".
  * debian/control (python-indigo/Depends): Bump libindigo0d version to 1.1.
  * debian/control (libindigo-java/Depends): Likewise.

  [ Daniel Leidert ]
  * debian/watch: Added.

 -- Michael Banck <mbanck@debian.org>  Fri, 04 May 2012 01:13:03 +0200

indigo (1.0.0-2) unstable; urgency=low

  * debian/rules: Add python2 to dh modules (Closes: #671057).

 -- Michael Banck <mbanck@debian.org>  Fri, 04 May 2012 00:39:01 +0200

indigo (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #664975).

 -- Michael Banck <mbanck@debian.org>  Sat, 24 Mar 2012 02:02:50 +0100
